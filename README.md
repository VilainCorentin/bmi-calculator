# BMI Calculator in Flutter

BMI calculator created with the Flutter framework.

# Screenshots

![alt text](https://gitlab.com/VilainCorentin/bmi-calculator/raw/master/screenshot/screen1.jpg "Screen 1")

![alt text](https://gitlab.com/VilainCorentin/bmi-calculator/raw/master/screenshot/screen2.jpg "Screen 2")