import 'package:flutter/material.dart';

const double margin = 15.0;
const double radiusBorder = 10.0;

class ReusableCard extends StatelessWidget {
  final Color colour;
  final Widget cardChild;
  final Function onTapFonction;

  ReusableCard({this.colour, this.cardChild, this.onTapFonction});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapFonction,
      child: Container(
        child: cardChild,
        margin: EdgeInsets.all(margin),
        decoration: BoxDecoration(
          color: colour,
          borderRadius: BorderRadius.circular(radiusBorder),
        ),
      ),
    );
  }
}
