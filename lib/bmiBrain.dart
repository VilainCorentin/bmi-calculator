import 'dart:math';

class BmiBrain {
  final int height;
  final int weight;

  double _bmi;

  BmiBrain({this.height, this.weight});

  String calculateBMI() {
    _bmi = weight / pow(height / 100, 2);
    return _bmi.toStringAsFixed(2);
  }

  String getResult() {
    if (_bmi >= 25) {
      return 'Overweight';
    } else if (_bmi > 18.5) {
      return 'Normal';
    } else {
      return 'Underweight';
    }
  }

  String getInterpretation() {
    if (_bmi >= 25) {
      return 'Stop eat';
    } else if (_bmi > 18.5) {
      return 'You\'re ok';
    } else {
      return 'Go eat';
    }
  }
}
